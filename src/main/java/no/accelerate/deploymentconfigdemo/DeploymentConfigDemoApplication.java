package no.accelerate.deploymentconfigdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeploymentConfigDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeploymentConfigDemoApplication.class, args);
    }

}
