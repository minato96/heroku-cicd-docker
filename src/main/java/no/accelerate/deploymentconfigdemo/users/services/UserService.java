package no.accelerate.deploymentconfigdemo.users.services;

import no.accelerate.deploymentconfigdemo.users.models.AppUser;

import java.util.Collection;

public interface UserService {
    Collection<AppUser> findAll();
    AppUser findById(int id);
    int add(AppUser user);
    void update(AppUser user);
    void delete(int id);
}
