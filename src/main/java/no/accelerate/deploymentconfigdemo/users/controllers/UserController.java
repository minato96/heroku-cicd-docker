package no.accelerate.deploymentconfigdemo.users.controllers;

import no.accelerate.deploymentconfigdemo.users.mappers.UserMapper;
import no.accelerate.deploymentconfigdemo.users.models.dtos.AppUserDTO;
import no.accelerate.deploymentconfigdemo.users.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("api/v2/users")
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;

    public UserController(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper; 
    }

    @GetMapping
    public ResponseEntity findAll() {
        return ResponseEntity.ok(userService.findAll());
    }

    @GetMapping("{id}")
    public ResponseEntity findOne(@PathVariable int id) {
        return ResponseEntity.ok(userService.findById(id));
    }

    @PostMapping
    public ResponseEntity add(@RequestBody AppUserDTO userDTO) {
        int id = userService.add(
                userMapper.userDtoToUser(userDTO)
        );
        URI uri = URI.create("api/v1/users/" + id);
        return ResponseEntity.created(uri).build();
    }

    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody AppUserDTO userDTO, int id) {
        if(id != userDTO.getId())
            return ResponseEntity.badRequest().build();
        userService.update(
                userMapper.userDtoToUser(userDTO)
        );
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        userService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
