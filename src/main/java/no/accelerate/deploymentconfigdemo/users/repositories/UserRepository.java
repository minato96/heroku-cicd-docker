package no.accelerate.deploymentconfigdemo.users.repositories;

import no.accelerate.deploymentconfigdemo.users.models.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<AppUser, Integer> {
}
